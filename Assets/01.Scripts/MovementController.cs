using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float movementSpeed = 3.0f;
    Vector2 movement = new Vector2();// new 생성자를 사용하는것은 초기화 하는것과 동일하다. new Vector2 -> Vector2.zero(0으로 초기화)도 가능 
    /*
     * 값 -> 구조체
     * 래퍼런스 -> 클래스
     */

    Animator animator; // 애니메이터 컴퍼넌트를 참조하기 위해 선언

    readonly string animationsState = "AnimationsState";
    //UpdateState에서 값을 변경할때 해당 변수명만 바꿔주면 편리하다
    //readonly를 사용해주면 가비지(쓰레기값)이 발생하지 않는다.

    Rigidbody2D rb2D;


    enum CharStates //열거형 스테이트머신의 기초형
        //enum값은 정수형이 아닌 열거형이다. 별도의 타입 따라서 int형으로 형변환
    {
        //각 하나하나의 상태를 정의함,(1값일땐 2가 안되고 2값일땐 3이 안된다)
        WalkEast    = 1,
        WalkSouth   = 2,
        WalkWest    = 3,
        WalkNorth   = 4,
        idleSouth   = 5,
    }


    private void Start()
    {
        animator = this.GetComponent<Animator>(); // 
        rb2D = this.GetComponent<Rigidbody2D>(); // Unity에서의 this는 해당 Script가 Component된 Object
    }

    void Update()
    {
        UpdateState();
    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }

    private void MoveCharacter()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        movement.Normalize(); // Normalize -> 

        rb2D.velocity/*실제이동*/ = movement/*순수방향값*/ * movementSpeed;/*속력(힘)*/
        //velocity로 인하여 FixedUpdate에서 움직임을 제어
        //velocity 현재 위치에서의 이동속도
    }

    private void UpdateState() //사실상 해당 메소드가 UpdateAnimationState이다. 가독성을 위해 별도메소드로 관리
    {
        if(movement.x > 0) // 오른쪽으로 이동
        {
            animator.SetInteger(animationsState, (int)CharStates.WalkEast);
        }

        else if(movement.x < 0)//왼쪽으로 이동
        {
            animator.SetInteger(animationsState, (int)CharStates.WalkWest);
        }

        else if(movement.y > 0)//위쪽으로 이동
        {
            animator.SetInteger(animationsState, (int)CharStates.WalkNorth);
        }

        else if(movement.y < 0)//아래쪽으로 이동
        {
            animator.SetInteger(animationsState, (int)CharStates.WalkSouth);
        }
        else//키입력이 없을때
        {
            animator.SetInteger(animationsState, (int)CharStates.idleSouth);
        }
    }
}
